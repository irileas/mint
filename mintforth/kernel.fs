: variable create 0 , ;
: constant create , does> @ ;

: compiling?  compiling @ ;

: 1+     1 + ;
: 1-     1 - ;
: inc!   dup @ 1+ swap ! ;

: bool   if true  else false then ;
: not    if false else true  then ;
: logor  or bool ;
: logand bool swap bool and ;

: over   >r dup r> swap ;
: nip    swap drop ;
: tuck   swap over ;
: rot    >r swap r> swap ;

: 2drop  drop drop ;
: 2dup   over over ;

: c@     @ 255 and ;
: c!
   swap 255 and
   over @  255 invert  and 
   or  swap !
;

: <> = not ;
: <= 2dup = if 2drop true else < then ;
: >= 2dup = if 2drop true else > then ;

: input?     in> <in < ;
: this-char  in> @ c@ ;
: advance
   input? if
      in> inc!
   else
      1 throw
   then
;
: next-char  this-char advance ;

: \ immediate
   begin
      input? if
         this-char 10 <>
      else
         false
      then
   while
      advance
   repeat
;

: space=?
   dup  32 =
   over 10 = logor
   over  9 = logor
   nip
;

: wsskip  \ Skip whitespace
   begin
      input? if
         this-char space=?
      else
         false
      then
   while
      advance
   repeat
;

: skip  \ ( char -- )
   advance  \ Skip space after word
   begin
      dup next-char <>
   until
   drop
;

: '  word find  1 <> if 1 throw then ;
: ['] immediate ' , ;
: literal immediate ['] (lit) , ;

: (char) wsskip next-char ;
: char immediate (char) ;
: [char] immediate (char) literal ;

: ( immediate
  char ) skip
;

