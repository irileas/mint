;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MINTForth
;;;   by Irileas
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


        .org 512
RETURN_STACK_TOP:
        
        .org 1024
DATA_STACK_TOP:

        .org 2048
        .reg IAR main


;;; Important Constants and Variables
        ;; Stack Constants
DATA_STACK_TOP_VALUE:   LDC DATA_STACK_TOP
RETURN_STACK_TOP_VALUE: LDC RETURN_STACK_TOP
        
        ;; Number Constants
ZERO:                   .lit 0x00000000
ONE:                    .lit 0x00000001
FOUR:                   .lit 0x00000004
C_000000FF:             .lit 0x000000ff
C_FFFFFF00:             .lit 0xffffff00
C_7FFFFFFF:             .lit 0x7fffffff
C_FFFFFFFF:             .lit 0xffffffff

        ;; I/O Constants
SPACE_CHAR:             .lit 32
NEWLINE_CHAR:           .lit 10
MINUS_CHAR:		.lit 45
TEXT_IO_ADDRESS:        .lit 0xffffffff
TIB_LENGTH:             .lit 80
IMMEDIATE_FLAG_MASK:    .lit 0x01

        ;; Configuration Constants
FIRST_FORTH_WORD:       LDC DOES_WORD

        ;; Errors
ERROR_WORD_NOT_FOUND:           .lit 0
ERROR_COMPILE_ONLY:             .lit 1
ERROR_DATA_STACK_UNDERFLOW:     .lit 2
ERROR_DATA_STACK_OVERFLOW:      .lit 3
ERROR_RETURN_STACK_UNDERFLOW:   .lit 4
ERROR_RETURN_STACK_OVERFLOW:    .lit 5

        ;; Global Variables
tmp_:                   .lit 0  ; Multi-purpose variable
tmp2_:                  .lit 0  ; Multi-purpose variable
tmp3_:                  .lit 0  ; Multi-purpose variable
ip_:                    .lit 0  ; Instruction pointer
allot_ptr_:             .lit 0  ; Allot pointer
tib_:                   .lit 0  ; TIB variable
numbertib_:             .lit 0  ; #TIB variable
in_:                    .lit 0  ; IN> variable
in_end_:                .lit 0  ; IN< variable
compiling_:             .lit 0  ; COMPILING? variable
dictionary_:            .lit 0  ; Current dictionary head

        ;; Local Variables
accept__counter_:       .lit 0
find__dictptr_:         .lit 0
find__number_:          .lit 0
find__counter_:         .lit 0
find__number_negative_:	.lit 0
type_string__l_:        .lit 0
type_string__p_:        .lit 0
type_string__c_:        .lit 0
string_compare__s1p_:   .lit 0
string_compare__s1l_:   .lit 0
string_compare__s2p_:   .lit 0
string_compare__s2l_:   .lit 0
comma__value_:          .lit 0
create__counter_:       .lit 0
create__straddr_:       .lit 0
create__strlen_:        .lit 0
mult__result_:          .lit 0
mult__a_:               .lit 0
mult__b_:               .lit 0
div__result_:           .lit 0
div__counter_:          .lit 0
div__a_:                .lit 0
div__b_:                .lit 0


;;; Stack Operations

dpeek:
        LDSP
        EQL DATA_STACK_TOP_VALUE
        JMN dstack_underflow
	LDRS 0
	RET

dpush:
	STRS -4
	LDSP
        EQL RETURN_STACK_TOP_VALUE
        JMN dstack_overflow
        LDSP
	ADC -4
	STSP
	LDRS 0
	RET

dpop:
        LDSP
        EQL DATA_STACK_TOP_VALUE
        JMN dstack_underflow
	LDSP
	ADC 4
	STSP
	LDRS -4
	RET

rpeek:
        LDFP
        EQL RETURN_STACK_TOP_VALUE
        JMN rstack_underflow
	LDRF 0
	RET

rpush:
	STRF -4
	LDFP
        EQL FOUR
        JMN rstack_overflow
        LDFP
	ADC -4
	STFP
	LDRF 0
	RET

rpop:
        LDFP
        EQL RETURN_STACK_TOP_VALUE
        JMN rstack_underflow
	LDFP
	ADC 4
	STFP
	LDRF -4
	RET


        .str "DROP"
        .lit 4
        .byte 0
        LDC DUP_WORD
DROP_WORD:
        CALL dpop
        JMP next

        .str "DUP"
        .lit 3
        .byte 0
        LDC SWAP_WORD
DUP_WORD:
        CALL dpeek
        CALL dpush
        JMP next

        .str "SWAP"
        .lit 4
        .byte 0
        LDC TO_R_WORD
SWAP_WORD:
        CALL dpop
        STV tmp_
        CALL dpop
        STV tmp2_
        LDV tmp_
        CALL dpush
        LDV tmp2_
        CALL dpush
        JMP next
        
        .str ">R"
        .lit 2
        .byte 0
        LDC FROM_R_WORD
TO_R_WORD:
        CALL dpop
        CALL rpush
        JMP next

        .str "R>"
        .lit 2
        .byte 0
        LDC PLUS_WORD
FROM_R_WORD:
        CALL rpop
        CALL dpush
        JMP next



;;; Math Routines

mul10_var_: .lit 0
mul10:
        STV mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        ADD mul10_var_
        RET
        
        
        .str "+"
        .lit 1
        .byte 0
        LDC DIVISION_WORD
PLUS_WORD:
        CALL dpop
        STV tmp_
PLUS_WORD_add_part:
        CALL dpop
        ADD tmp_
        CALL dpush
        JMP next

        .str "-"
        .lit 1
        .byte 0
        LDC INVERT_WORD
MINUS_WORD:
        CALL dpop
        NOT
        ADC 1
        STV tmp_
        JMP PLUS_WORD_add_part

        .str "*"
        .lit 1
        .byte 0
        LDC MINUS_WORD
MULTIPLICATION_WORD:
        LDC 0
        STV mult__result_
        CALL dpop
        STV mult__a_
        CALL dpop
        STV mult__b_
mult__loop:                     ; Expecting mult__b_ to be loaded
        EQL ZERO
        JMN mult__loop_end
        LDV mult__b_            ; If b is odd
        AND ONE
        EQL ZERO
        JMN mult__loop_b_not_odd
        LDV mult__a_            ; Add A to the result
        ADD mult__result_
        STV mult__result_
mult__loop_b_not_odd:
        LDV mult__a_            ; Left-shift A
        ADD mult__a_
        STV mult__a_
        LDV mult__b_            ; Right-shift B
        RAR
        AND C_7FFFFFFF
        STV mult__b_
        JMP mult__loop          ; Continue the loop
mult__loop_end:
        LDV mult__result_
        CALL dpush
        JMP next

        .str "/"
        .lit 1
        .byte 0
        LDC MULTIPLICATION_WORD
DIVISION_WORD:
        CALL dpop
        STV div__b_
        CALL dpop
        STV div__a_
        LDC 0
        STV div__result_
        STV div__counter_
div__loop:                      ; Expects COUNTER in ACC
        ADD div__b_             ; If a < (counter + b), break
        NOT
        ADC 1
        ADD div__a_
        JMN div__loop_end
        LDC 1                   ; result++
        ADD div__result_
        STV div__result_
        LDV div__counter_       ; counter += b
        ADD div__b_
        STV div__counter_
        JMP div__loop
div__loop_end:
        LDV div__result_
        CALL dpush
        JMP next
        

        .str "INVERT"
        .lit 6
        .byte 0
        LDC OR_WORD
INVERT_WORD:
        CALL dpop
        NOT
        CALL dpush
        JMP next

        .str "TRUE"
        .lit 4
        .byte 0
        LDC ACCEPT_WORD
TRUE_WORD:
        LDV C_FFFFFFFF
        CALL dpush
        JMP next

        .str "FALSE"
        .lit 5
        .byte 0
        LDC TRUE_WORD
FALSE_WORD:
        LDC 0
        CALL dpush
        JMP next

        .str "="
        .lit 1
        .byte 0
        LDC FALSE_WORD
EQL_WORD:
        CALL dpop
        STV tmp_
        CALL dpop
        EQL tmp_
        JMN TRUE_WORD
        JMP FALSE_WORD

        .str "<"
        .lit 1
        .byte 0
        LDC EQL_WORD
LESS_WORD:
        CALL dpop
        STV tmp_
        CALL dpop
        NOT
        ADD tmp_
        JMN FALSE_WORD
        JMP TRUE_WORD

        .str ">"
        .lit 1
        .byte 0
        LDC LESS_WORD
GREATER_WORD:
        CALL dpop
        STV tmp_
        CALL dpop
        STV tmp2_
        LDV tmp_
        NOT
        ADD tmp2_
        JMN FALSE_WORD
        JMP TRUE_WORD

        .str "AND"
        .lit 3
        .byte 0
        LDC GREATER_WORD
AND_WORD:
        CALL dpop
        STV tmp_
        CALL dpop
        AND tmp_
        CALL dpush
        JMP next

        .str "OR"
        .lit 2
        .byte 0
        LDC AND_WORD
OR_WORD:
        CALL dpop
        STV tmp_
        CALL dpop
        OR tmp_
        CALL dpush
        JMP next

        
        

;;; Input/Output Routines
        
getchar:
        LDIV TEXT_IO_ADDRESS
        AND C_000000FF
        RET


upcase__param_: .lit 0
upcase:
        STV upcase__param_
        ADC -97
        JMN upcase__no_upcase
        LDV upcase__param_
        ADC -122
        NOT
        JMN upcase__no_upcase
        LDV upcase__param_
        ADC -32
        RET
upcase__no_upcase:
        LDV upcase__param_
        RET


accept:
        LDRA                    ; Preserve the return address
        CALL rpush
        LDC 0x3e                ; Print prompt
        STIV TEXT_IO_ADDRESS
        LDC 0                   ; counter = 0;
        STV accept__counter_
        CALL dpop               ; buffer_end = buffer_pointer + length;
        STV tmp_
        CALL dpeek
        ADD tmp_
        STV tmp_
        CALL dpop               ; buffer = POP();
        STV tmp2_
accept__loop:                   ; Expects tmp2_ in ACC
        EQL tmp_                ; if (buffer == buffer_end)
        JMN accept__end         ;    return;
        LDIV tmp2_
        AND C_FFFFFF00
        STV tmp3_
        CALL getchar            ; *buffer = getchar()
        AND C_000000FF
        OR tmp3_
        STIV tmp2_
        AND C_000000FF          ; Normalize back to char
        EQL NEWLINE_CHAR        ; if (*buffer == '\n')
        JMN accept__end         ;   return;
        LDV accept__counter_    ; counter++;
        ADC 1
        STV accept__counter_
        LDV tmp2_               ; buffer++;
        ADC 1
        STV tmp2_
        JMP accept__loop        ; continue;
accept__end:
        CALL rpop
        STRA
        LDV accept__counter_
        RET
        
        .str "ACCEPT"
        .lit 6
        .byte 0
        LDC HERE_WORD
ACCEPT_WORD:
        CALL accept
        CALL dpush
        JMP next

        .str "EMIT"
        .lit 4
        .byte 0
        LDC DROP_WORD
EMIT_WORD:
        CALL dpop
        STIV TEXT_IO_ADDRESS
        JMP next



;;; String Processing

type_string:
        CALL dpop
        STV type_string__l_
        CALL dpop
        STV type_string__p_
        LDC 0
        STV type_string__c_
type_string__loop:
        EQL type_string__l_
        JMN type_string__end
        LDV type_string__c_
        ADD type_string__p_
        STV tmp_
        LDIV tmp_
        AND C_000000FF
        STIV TEXT_IO_ADDRESS
        LDV type_string__c_
        ADC 1
        STV type_string__c_
        JMP type_string__loop
type_string__end:
        RET

        
string_compare:  ; Returns 0xFFFFFFFF if successful, 0 otherwise!!!
        ;; First, we check whether the two strings have the same length
        LDRA
        CALL rpush
        LDV string_compare__s1l_
        EQL string_compare__s2l_
        JMN string_compare__start_loop
string_compare__fail:
        CALL rpop
        STRA
        LDC 0
        RET
string_compare__start_loop:
        LDC 0                           ; Initialize loop: S2L = 0
        STV string_compare__s2l_
string_compare__loop:                   ; We assume that S2L is loaded
        EQL string_compare__s1l_
        JMN string_compare__successful  ; We have reached the end, success!
        LDV string_compare__s1p_        ; TMP = S1P[S2L]
        ADD string_compare__s2l_
        STV tmp_
        LDIV tmp_
        AND C_000000FF
        CALL upcase     
        STV tmp_
        LDV string_compare__s2p_        ; TMP2 = S2P[S2L]
        ADD string_compare__s2l_
        STV tmp2_
        LDIV tmp2_
        AND C_000000FF
        CALL upcase
        EQL tmp_                        ; If TMP != TMP2, fail
        NOT
        JMN string_compare__fail
        LDV string_compare__s2l_        ; Increment S2L
        ADC 1
        STV string_compare__s2l_
        JMP string_compare__loop        ; Repeat
string_compare__successful:
        CALL rpop
        STRA
        LDV C_FFFFFFFF
        RET

        

;;; Memory Allocation

        .str "HERE"
        .lit 4
        .byte 0
        LDC ALLOT_WORD
HERE_WORD:
        LDV allot_ptr_
        CALL dpush
        JMP next

allot:
        ADD allot_ptr_
        STV allot_ptr_
        RET
        
        .str "ALLOT"
        .lit 5
        .byte 0
        LDC FETCH_WORD
ALLOT_WORD:
        CALL dpop
        CALL allot
        JMP next

        .str "@"
        .lit 1
        .byte 0
        LDC STORE_WORD
FETCH_WORD:
        CALL dpop
        STV tmp_
        LDIV tmp_
        CALL dpush
        JMP next

        .str "!"
        .lit 1
        .byte 0
        LDC COMMA_WORD
STORE_WORD:
        CALL dpop
        STV tmp_
        CALL dpop
        STIV tmp_
        JMP next


comma:
comma_from_acc:
        STV comma__value_
        LDRA
        CALL rpush
        LDV comma__value_
        JMP comma__body
comma_from_stack:
        LDRA
        CALL rpush
        CALL dpop
comma__body:
        STIV allot_ptr_
        LDV allot_ptr_
        ADC 4
        STV allot_ptr_
        CALL rpop
        STRA
        RET

comma1_from_acc:
        AND C_000000FF
        STV comma__value_
        LDV allot_ptr_
        AND C_FFFFFF00
        OR comma__value_
        STIV allot_ptr_
        LDV allot_ptr_
        ADC 1
        STV allot_ptr_
        RET
        
        .str ","
        .lit 1
        .byte 0
        LDC ELSE_WORD
COMMA_WORD:
        CALL comma_from_stack
        JMP next



;;; Compilation and Control Structures

        .str "(LIT)"
        .lit 3
        .byte 0
        LDC NUMBERTIB_WORD
LIT_WORD:
        LDIV ip_
        CALL dpush
        LDV ip_
        ADC 4
        STV ip_
        JMP next

        .str "(BRANCH)"
        .lit 6
        .byte 0
        LDC LIT_WORD
BRANCH_WORD:
        LDIV ip_
        JMP next_from_acu

        .str "(?BRANCH)"
        .lit 6
        .byte 0
        LDC BRANCH_WORD
BRANCH_CONDITIONAL_WORD:        ; Branches if zero
        CALL dpop
        EQL ZERO
        JMN BRANCH_WORD
        LDV ip_
        ADC 4
        STV ip_
        JMP next

        .str "(?TBRANCH)"
        .lit 7
        .byte 0
        LDC BRANCH_CONDITIONAL_WORD
BRANCH_ON_TRUE_CONDITIONAL_WORD:
        CALL dpop
        EQL ZERO
        NOT
        JMN BRANCH_WORD
        LDV ip_
        ADC 4
        STV ip_
        JMP next

        .str "BEGIN"
        .lit 5
        .byte 0x01
        LDC BRANCH_ON_TRUE_CONDITIONAL_WORD
BEGIN_WORD:
        CALL check_compile_only
        LDV allot_ptr_
        CALL dpush
        JMP next

        .str "AGAIN"
        .lit 5
        .byte 0x01
        LDC BEGIN_WORD
AGAIN_WORD:
        CALL check_compile_only
        LDC BRANCH_WORD
        CALL comma_from_acc
        CALL dpop
        CALL comma_from_acc
        JMP next

        .str "UNTIL"
        .lit 5
        .byte 0x01
        LDC AGAIN_WORD
UNTIL_WORD:
        CALL check_compile_only
        LDC BRANCH_ON_TRUE_CONDITIONAL_WORD
        CALL comma_from_acc
        CALL dpop
        CALL comma_from_acc
        JMP next

        .str "WHILE"
        .lit 5
        .byte 0x01
        LDC UNTIL_WORD
WHILE_WORD:
        JMP IF_WORD             ; Basically the same

        .str "REPEAT"
        .lit 6
        .byte 0x01
        LDC WHILE_WORD
REPEAT_WORD:
        CALL check_compile_only
        CALL dpop               ; TMP contains pointer to WHILE gap
        STV tmp_
        LDC BRANCH_WORD         ; Write a branch to BEGIN
        CALL comma_from_acc
        CALL dpop
        CALL comma_from_acc
        LDV allot_ptr_          ; Fill the gap left by WHILE
        STIV tmp_
        JMP next

        .str "IF"
        .lit 2
        .byte 0x01               ; "Immediate"
        LDC REPEAT_WORD
IF_WORD:
        CALL check_compile_only
        LDC BRANCH_CONDITIONAL_WORD ; Write a branch
        CALL comma_from_acc
        LDV allot_ptr_          ; Push the location of the gap for THEN
        CALL dpush
        CALL comma_from_acc     ; Compile a dummy target, TODO: Point to trap
        JMP next

        .str "THEN"
        .lit 4
        .byte 0x01               ; "Immediate"
        LDC IF_WORD
THEN_WORD:
        CALL check_compile_only
        CALL dpop               ; Fill in the gap left by IF
        STV tmp_
        LDV allot_ptr_
        STIV tmp_
        JMP next

        .str "ELSE"
        .lit 4
        .byte 0x01               ; "Immediate"
        LDC THEN_WORD
ELSE_WORD:
        CALL check_compile_only
        CALL dpop               ; Get the location of the gap
        STV tmp_
        LDC BRANCH_WORD
        CALL comma_from_acc
        LDV allot_ptr_          ; Push location of gap
        CALL dpush
        CALL comma_from_acc     ; Compile a dummy target (TODO: point to trap)
        LDV allot_ptr_          ; Fill in the last gap
        STIV tmp_
        JMP next
        
        
        .str ":"
        .lit 1
        .byte 0
        LDC CREATE_WORD
COLON_WORD:
        CALL create             ; Create the header, the word address is in the ACU
        ADC 4                   ; Skip the "CALL enter" instruction
        STV allot_ptr_          ; Start compiling after "CALL enter"
        LDV C_FFFFFFFF          ; Enable the compiler
        STV compiling_
        JMP next
COLON__CALL_ENTER:
        CALL enter

        .byte 0x3b              ; ";"
        .lit 1
        .byte 0x01              ; Immediate
        LDC COLON_WORD
SEMICOLON_WORD:
        LDC EXIT_WORD
        CALL comma_from_acc
        LDC 0
        STV compiling_
        JMP next


        .str "]"
        .lit 1
        .byte 0x01              ; Immediate
        LDC SEMICOLON_WORD
START_COMPILING_WORD:
        LDV C_FFFFFFFF
        STV compiling_
        JMP next

        .str "["
        .lit 1
        .byte 0x01
        LDC START_COMPILING_WORD
STOP_COMPILING_WORD:
        LDC 0
        STV compiling_
        JMP next
        
        .str "IMMEDIATE"
        .lit 9
        .byte 0x01              ; Immediate is immediate ;-)
        LDC STOP_COMPILING_WORD
IMMEDIATE_WORD:
        CALL check_compile_only ; Avoid calling while not in compile mode
        LDV dictionary_         ; Load latest word
        ADC -5                  ; Get address of flags
        STV tmp_
        LDIV tmp_               ; Get flags
        OR IMMEDIATE_FLAG_MASK  ; Set immediate flag
        STIV tmp_               ; Store flags
        JMP next
        
        .str "DOES>"
        .lit 5
        .byte 0x01              ; "Immediate"
        LDC IMMEDIATE_WORD
DOES_WORD:
        LDV compiling_          ; The compile mode behavior differs from the
        EQL ZERO                ; execute mode behavior. Therefore, we branch.
        JMN DOES__CALL_COMPILER ; In execute mode we call the compiler directly
        LDC LIT_WORD
        CALL comma_from_acc
        LDV allot_ptr_
        ADC 12
        CALL comma_from_acc
        LDC DOES__COMPILER      ; In compile mode we postpone it until the
        CALL comma_from_acc     ; word we're currently compiling is run.
        LDC EXIT_WORD           ; This prevents our creator word to call the
        CALL comma_from_acc     ; code after DOES>
        JMP next
DOES__CALL_COMPILER:
        LDV allot_ptr_          ; The code our word will jump to starts right
        CALL dpush              ; after HERE
        LDV C_FFFFFFFF          ; Enter compile mode
        STV compiling_
        JMP DOES__COMPILER      ; Call the DOES> compiler
        .str ""
        .lit 0
        .byte 0
        .lit 0
DOES__COMPILER:                ; This is a pseudoword
        LDV dictionary_        ; Fetch last word from dictionary
        ADC 12                 ; Go to last address of its sentinel footer
        STV tmp_
        LDC BRANCH_WORD        ; Insert a BRANCH instruction
        STIV tmp_
        LDV tmp_               ; Increment the pointer variable to
        ADC 4                  ; insert a parameter for BRANCH
        STV tmp_
        CALL dpop              ; Insert the parameter from the stack
        STIV tmp_
        JMP next


;;; Word Memory Layout, Dictionaries and CREATE

get_flags:
        ADC -5
        STV tmp_
        LDIV tmp_
        AND C_000000FF
        RET

get_link:
        ADC -4
        STV tmp_
        LDIV tmp_
        RET


create:
        LDRA                    ; Preserve return address
        CALL rpush
        CALL word               ; Parse the next word
        CALL dpop
        STV create__strlen_     ; Store the word length, TODO: Check if not null!
        CALL dpop
        STV create__straddr_
        LDC 0
        STV create__counter_
create__loop:
        EQL create__strlen_
        JMN create__header
        LDV create__counter_
        ADD create__straddr_
        STV tmp_
        LDIV tmp_
        AND C_000000FF
        CALL comma1_from_acc
        LDV create__counter_
        ADC 1
        STV create__counter_
        JMP create__loop
create__header:
        LDV create__strlen_     ; Write string length
        CALL comma_from_acc
        LDC 0                   ; Write flags
        CALL comma1_from_acc
        LDV dictionary_         ; Write dictionary link
        CALL comma_from_acc
        LDV allot_ptr_          ; Link word into dictionary
        STV dictionary_
        ;; Write a footer, which causes every new word to push the
        ;; address of its data section.
        ;; The DOES> word modifies this footer to do its magic.
        LDV CREATE__CALL_ENTER_
        CALL comma_from_acc
        LDC LIT_WORD
        CALL comma_from_acc
        LDV allot_ptr_          ; The address of the data section
        ADC 12
        CALL comma_from_acc
        LDC EXIT_WORD           ; Compile the EXIT routine
        CALL comma_from_acc
        LDC 0                   ; Add one word to provide space for a branch
        CALL comma_from_acc
        CALL rpop               ; Return
        STRA
        LDV dictionary_         ; Return the created word
        RET
CREATE__CALL_ENTER_:            ; Sentinel value, will be added after new word
        CALL enter
        
        
        .str "CREATE"
        .lit 6
        .byte 0
        LDC EMIT_WORD
CREATE_WORD:
        CALL create
        JMP next



;;; FIND

find:
        LDRA                    ; Preserve the return address
        CALL rpush
        CALL dpop               ; Store the passed string
        STV string_compare__s1l_
        CALL dpop
        STV string_compare__s1p_
        LDV dictionary_         ; Initialize find__dictptr_ with the dictionary
        STV find__dictptr_
find__loop:                     ; Assuming find__dictptr_ is loaded, pointing to a dict
        EQL ZERO                ; If the dictionary is empty, fail
        JMN find__unsuccessful
        LDV find__dictptr_     ; Load word name into string_compare variables
        ADC -9
        STV tmp2_
        LDIV tmp2_
        STV string_compare__s2l_
        NOT
        ADC -8
        ADD find__dictptr_
        STV string_compare__s2p_
        CALL string_compare     ; Compare name
        NOT                     ; Change the result: -1 now stands for "unsuccessful"
        JMN find__try_next      ; If unsuccessful, advance pointer
        LDV find__dictptr_      ; Success! Push our word
        CALL dpush
        LDC 1                   ; And push the number 1 ("word found")
        CALL dpush
        JMP find__end
find__try_next:
        LDV find__dictptr_      ; tmp = tmp->link
        CALL get_link
        STV find__dictptr_
        JMP find__loop          ; Restart the loop
find__unsuccessful:
        ;; Try parsing a number
        LDC 0
        STV find__number_
find__number:
	LDV string_compare__s1l_
	EQL ZERO
	JMN find__fail
	LDIV string_compare__s1p_ ; Load first char
	AND C_000000FF
	EQL MINUS_CHAR		; If it's a minus
	JMN find__number_negative
	LDC 0
	STV find__number_negative_
	LDC 0
	JMP find__number_init_loop
find__number_negative:
	LDV C_FFFFFFFF
	STV find__number_negative_
	LDC 1
	JMP find__number_init_loop
find__number_init_loop:
	STV find__counter_
find__number_loop:              ; Expect find__counter_
        EQL string_compare__s1l_
        JMN find__number_success
        LDV string_compare__s1p_
        ADD find__counter_
        STV tmp_
        LDIV tmp_
        AND C_000000FF
        ADC -48
        JMN find__fail
        STV tmp_
        ADC -10
        NOT
        JMN find__fail
        LDV find__number_
        CALL mul10
        ADD tmp_
        STV find__number_
        LDV find__counter_
        ADC 1
        STV find__counter_
        JMP find__number_loop
find__number_success:
	LDV find__number_negative_
	NOT
	JMN find__push_number
	LDV find__number_	; Convert to negative
	NOT
	ADC 1
	STV find__number_
find__push_number:
        LDV find__number_
        CALL dpush
        LDV C_FFFFFFFF
        CALL dpush
        JMP find__end
find__fail:
        LDC 0
        CALL dpush
find__end:
        CALL rpop
        STRA
        RET

        .str "FIND"
        .lit 4
        .byte 0
        LDC COLD_WORD
FIND_WORD:
        CALL find
        JMP next


;;; Parsing and Input
        
        ;; "word" tries to parse the next space-delimited name from the
        ;; current input buffer (denoted by IN> and IN<). The result
        ;; is a counted string (on the stack).
        ;; It advances IN> to the end of the word name in
        ;; the buffer (skipping spaces) and returns
        ;; the word as a counted string on the stack.
        ;; MODIFIES: ACC in_ tmp_ dstack
word:
        LDRA                    ; Save return address
        CALL rpush
        LDC 0                   ; Clear TMP
        STV tmp_
        CALL dpush              ; Push a dummy return value on the stack
        LDV in_
word__read_spaces:
        ;; TODO: subtract and check if less?
        EQL in_end_             ; Compare IN> to IN<
        JMN word__end           ; If equal, terminate with result 0  TODO: This breaks! No return value!
        LDIV in_                ; Load character pointed to by IN>
        AND C_000000FF
        EQL SPACE_CHAR          ; If the character is not a space
        NOT
        JMN word__read_chars    ; Begin accumulating the characters
        LDV in_                 ; Increment IN>
        ADC 1
        STV in_
        JMP word__read_spaces   ; Repeat
word__read_chars:
        CALL dpop               ; Drop dummy value
        LDV in_                 ; We store IN> on the stack
        CALL dpush
word__read_chars_loop:
        ;; TODO: subtract and check if less?
        EQL in_end_             ; Compare IN> to IN<
        JMN word__end           ; If equal, terminate with result TMP
        LDIV in_                ; Load character pointed to by IN>
        AND C_000000FF
        EQL SPACE_CHAR          ; If the character is a space
        JMN word__end           ; Terminate with result TMP
        LDV tmp_                ; Increment the word length counter
        ADC 1
        STV tmp_
        LDV in_                 ; Increment the buffer pointer
        ADC 1
        STV in_
        JMP word__read_chars_loop ; Repeat
word__end:
        LDV tmp_                ; Push counter, leaving us with a counted
        CALL dpush              ; string on the stack
        CALL rpop               ; Restore the return address
        STRA
        RET


        .str "WORD"
        .lit 4
        .byte 0
        LDC FIND_WORD
WORD_WORD:
        CALL word
        JMP next

        .str "IN>"
        .lit 3
        .byte 0
        LDC IN_END_WORD
IN_BEGIN_WORD:
        LDC in_
        CALL dpush
        JMP next

        .str "<IN"
        .lit 3
        .byte 0
        LDC WORD_WORD
IN_END_WORD:
        LDC in_end_
        CALL dpush
        JMP next
        
        .str "TIB"
        .lit 3
        .byte 0
        LDC IN_BEGIN_WORD
TIB_WORD:
        LDV tib_
        CALL dpush
        JMP next

        .str "#TIB"
        .lit 4
        .byte 0
        LDC TIB_WORD
NUMBERTIB_WORD:
        LDV numbertib_
        CALL dpush
        JMP next



;;; Quit and Abort
        
        .str ""
        .lit 0
        .byte 0
        .lit 0
QUIT_PSEUDOWORD:
        JMP quit__inner_loop

quit_code_vector_:
        .lit 0
        LDC QUIT_PSEUDOWORD

	.str "QUIT"
	.lit 4
	.byte 0
	LDC EXECUTE_WORD
QUIT_WORD:
quit:   
	LDC RETURN_STACK_TOP
	STFP
quit__loop:
        LDV tib_                        ; Push address of TIB
        CALL dpush
        LDV TIB_LENGTH                  ; Push length of TIB
        CALL dpush
        LDV tib_                        ; Store TIB in IN>
        STV in_
        CALL accept                     ; Read a string
        STV numbertib_                  ; Store the length in #TIB
        ADD in_                         ; Calculate IN<
        STV in_end_
quit__inner_loop:
        CALL word                       ; Parse the next word
        CALL dpeek                      ; Get the word length
        EQL ZERO                        ; If there is no word in the input buffer,
        JMN quit__break_inner_loop      ; break the loop and read the next line
        CALL find                       ; Call FIND with the results on the stack
        CALL dpop                       ; Result is in ACC (-1=number, 0=not found, 1=found)
        JMN quit__compile_number        ; If the result is negative (a number is on the stack)
        EQL ZERO                        ; If the result is zero (not found)
        JMN quit__word_not_found        ; Throw an error
        ;; Compile/Execute the word
        CALL dpeek                      ; Load the word address into ACC
        CALL get_flags                  ; Load the flags
        NOT                             ; Invert the flags ("not immediate")
        AND IMMEDIATE_FLAG_MASK         ; Extract the IMMEDIATE flag
        AND compiling_                  ; "Compiling and word is not immediate"
        EQL ZERO                        ; If this is not the case
        JMN quit__execute               ; Execute the word
        CALL comma_from_stack           ; Compile the word
        JMP quit__inner_loop            ; And resume the loop
quit__execute:
        CALL dpop
        STV quit_code_vector_
        LDC quit_code_vector_
        STV ip_
        CALL next
quit__compile_number:
        LDV compiling_                  ; Check if we're compiling
        EQL ZERO                        ; If we're not compiling,
        JMN quit__inner_loop            ; leave the number on the stack
        LDC LIT_WORD                    ; Compile a literal
        CALL comma_from_acc
        CALL comma_from_stack
        JMP quit__inner_loop
quit__word_not_found:
        LDV ERROR_WORD_NOT_FOUND
        JMP throw
quit__break_inner_loop:
        CALL dpop               ; Drop the name length
        CALL dpop               ; Drop the name address
        JMP quit__loop          ; Return to the loop

throw:                          ; ACC contains the error code
        LDC 0x21                ; Print an exclamation mark
        STIV TEXT_IO_ADDRESS
        LDC 0x0a
        STIV TEXT_IO_ADDRESS
        LDC 0
        STV compiling_
        JMP abort               ; TODO: Exception handlers?
throw_compile_only:
        LDV ERROR_COMPILE_ONLY
        JMP throw
dstack_underflow:
        LDV ERROR_DATA_STACK_UNDERFLOW
        JMP throw
dstack_overflow:
        LDV ERROR_DATA_STACK_OVERFLOW
        JMP throw
rstack_underflow:
        LDV ERROR_RETURN_STACK_UNDERFLOW
        JMP throw
rstack_overflow:
        LDV ERROR_RETURN_STACK_OVERFLOW
        JMP throw

check_compile_only:
        LDV compiling_
        EQL ZERO
        JMN throw_compile_only
        RET

        .str "THROW"
        .lit 5
        .byte 0
        LDC QUIT_WORD
THROW_WORD:
        CALL dpop
        JMP throw

	.str "ABORT"
	.lit 5
	.byte 0
	LDC THROW_WORD
ABORT_WORD:
abort:
	LDC DATA_STACK_TOP
	STSP
	JMP QUIT_WORD



;;; Inner Interpreter

	.str "EXECUTE"
	.lit 7
	.byte 0
	LDC ENTER_WORD
EXECUTE_WORD:
execute:
	CALL dpop
        STRA
        RET


	.str "ENTER"
	.lit 5
	.byte 0
	LDC EXIT_WORD
ENTER_WORD:
;; Threaded words have to start with the instruction "CALL enter"
enter:
	LDRA
	STV tmp_
	LDV ip_
	CALL rpush
	LDV tmp_
	JMP next_from_acu


	.str "EXIT"
	.lit 4
	.byte 0
        LDC NEXT_WORD
EXIT_WORD:
exit:
        ;; Check if the return stack is empty. If this is the case,
        ;; return to the prompt ("QUIT").
        LDFP
        STV tmp_
        LDC RETURN_STACK_TOP
        EQL tmp_
        JMN quit__inner_loop
	CALL rpop
	JMP next_from_acu


	.str "NEXT"
	.lit 4
	.byte 0
	.lit 0
NEXT_WORD:
	JMP next
next_from_acu:
	STV ip_
next:
	LDIV ip_
	STRA
	LDV ip_
	ADC 4
	STV ip_
	RET



;;; Initialization and Code Entry

        .str "BYE"
        .lit 3
        .byte 0
        LDC ABORT_WORD
BYE_WORD:
        HALT
        
	.str "COLD"
	.lit 4
	.byte 0
	LDC BYE_WORD
COLD_WORD:
main:
        LDC DATA_BEGIN          ; Initialize ALLOT pointer
        STV allot_ptr_
        LDV allot_ptr_          ; Initialize: TIB = HERE
        STV tib_
        LDV TIB_LENGTH          ; Allocate TIB_LENGTH bytes
        CALL allot
        LDV FIRST_FORTH_WORD    ; Initialize dictionary pointer
        STV dictionary_
	JMP abort               ; Clear data stacks and start interpreter


;;; Start of bootstrap code written in FORTH
FORTHSTRAP_BEGIN:
        .file "kernel.fs"
FORTHSTRAP_END:

;;; Start of Memory Section

DATA_BEGIN:                     ; This label must be pointing to the end of the program
