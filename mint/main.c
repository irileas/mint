#include <stdio.h>
#include <stdint.h>
#include <string.h>


typedef uint32_t nima_word_t;
typedef uint8_t byte_t;


int load_word(FILE *file, nima_word_t *target)
{
    unsigned short i;
    unsigned char ch;
    nima_word_t word;

    word = 0;
    for (i = 0; i < 4; i++) {
        if (!fread(&ch, 1, 1, file)) return 0;
        word |= ((nima_word_t) ch) << (i * 8);
    }

    *target = word;
    return 1;
}


void save_word(FILE *file, nima_word_t word)
{
    unsigned char ch;
    ch = (word);        fwrite(&ch, 1, 1, file);
    ch = (word >>  8);  fwrite(&ch, 1, 1, file);
    ch = (word >> 16);  fwrite(&ch, 1, 1, file);
    ch = (word >> 24);  fwrite(&ch, 1, 1, file);
}



struct nima_state
{
    nima_word_t io_register;
    nima_word_t IAR;
    nima_word_t ACC;
    nima_word_t RA;
    nima_word_t SP;
    nima_word_t FP;
    byte_t MEMORY[0x1000000];
};


void nima_setup(struct nima_state *nima)
{
    memset(&(nima->MEMORY), 0, sizeof(nima->MEMORY));
    nima->io_register = ~0;  // Can not be reached
}

nima_word_t nima_getmem(struct nima_state *nima, nima_word_t addr)
{
    if (addr == nima->io_register) {
        return getchar();
    } else {
        return (nima->MEMORY[addr + 3] << 24)
            | (nima->MEMORY[addr + 2] << 16)
            | (nima->MEMORY[addr + 1] << 8)
            | (nima->MEMORY[addr]);
    }
}

void nima_setmem(struct nima_state *nima, nima_word_t addr, nima_word_t value)
{
    if (addr == nima->io_register) {
        putchar(value);
        fflush(stdout);
    } else {
        nima->MEMORY[addr + 3] = (value >> 24) & 0xff;
        nima->MEMORY[addr + 2] = (value >> 16) & 0xff;
        nima->MEMORY[addr + 1] = (value >> 8) & 0xff;
        nima->MEMORY[addr] = value & 0xff;
    }
}


void nima_save(struct nima_state *state, FILE *file)
{
    save_word(file, state->io_register);

    save_word(file, state->IAR);
    save_word(file, state->ACC);
    save_word(file, state->RA);
    save_word(file, state->SP);
    save_word(file, state->FP);

    fwrite(&state->MEMORY, 1, sizeof(state->MEMORY), file);
}


int nima_load(struct nima_state *state, FILE *file)
{
    unsigned int i;
    
    if (!(load_word(file, &state->io_register)
          && load_word(file, &state->IAR)
          && load_word(file, &state->ACC)
          && load_word(file, &state->RA)
          && load_word(file, &state->SP)
          && load_word(file, &state->FP))) {
        printf("Error: Invalid file format!\n");
        return 0;
    }
    
    for (i = 0; i < sizeof(state->MEMORY); i++) {
        if (!fread(&state->MEMORY[i], 1, 1, file))
            break;
    }

    return 1;
}


int nima_step(struct nima_state *nima)
{
    unsigned char opcode;
    nima_word_t instruction;
    nima_word_t operand;

    instruction = nima_getmem(nima, nima->IAR);
    nima->IAR += 4;
    
    if ((instruction & 0xf0000000) == 0xf0000000) {
        opcode = 0xf0 | ((instruction & 0x0f000000) >> 24);
        operand = (instruction & 0x00ffffff);
    } else {
        opcode = ((instruction & 0xf0000000) >> 28);
        operand = (instruction & 0x0fffffff);
    }

    /*printf("%08x: %02x %07x | %08x\n",
           nima->IAR - 1,
           opcode,
           operand,
           nima->ACC);*/

    switch (opcode) {
    case 0x00: nima->ACC = operand; break;
    case 0x01: nima->ACC = nima_getmem(nima, operand); break;
    case 0x02: nima_setmem(nima, operand, nima->ACC); break;
    case 0x03: nima->ACC = nima->ACC + nima_getmem(nima, operand); break;
    case 0x04: nima->ACC = nima->ACC & nima_getmem(nima, operand); break;
    case 0x05: nima->ACC = nima->ACC | nima_getmem(nima, operand); break;
    case 0x06: nima->ACC = nima->ACC ^ nima_getmem(nima, operand); break;
    case 0x07: {
        nima->ACC = (nima->ACC == nima_getmem(nima, operand)) ? -1 : 0;
        break;
    }
    case 0x08: nima->IAR = operand; break;
    case 0x09: if (nima->ACC & 0x8000000) nima->IAR = operand; break;
    case 0x0a: nima->ACC = nima_getmem(nima, nima_getmem(nima, operand)); break;
    case 0x0b: nima_setmem(nima, nima_getmem(nima, operand), nima->ACC); break;
    case 0x0c: nima->RA = nima->IAR; nima->IAR = operand; break;
    case 0x0d: {
        if (operand & 0x08000000) {
            nima->ACC = nima->ACC - ((~operand + 1) & 0x0fffffff);
        } else {
            nima->ACC = nima->ACC + operand;
        }
        break;
    }
    case 0xf0: nima->IAR--; return 0;
    case 0xf1: nima->ACC = ~(nima->ACC); break;
        // TODO: Don't shift, rotate!
    case 0xf2: nima->ACC = nima->ACC >> 1; break;
    case 0xf3: nima->IAR = nima->RA; break;
    case 0xf4: nima->ACC = nima->RA; break;
    case 0xf5: nima->RA = nima->ACC; break;
    case 0xf6: nima->ACC = nima->SP; break;
    case 0xf7: nima->SP = nima->ACC; break;
    case 0xf8: nima->ACC = nima->FP; break;
    case 0xf9: nima->FP = nima->ACC; break;
    case 0xfa: {
        nima->ACC = nima_getmem(nima,
                                (operand & 0x00800000)
                                ? (nima->SP - ((~operand + 1) & 0x00ffffff))
                                : (nima->SP + operand));
        break;
    }
    case 0xfb: {
        nima_setmem(nima,
                    (operand & 0x00800000)
                    ? (nima->SP - ((~operand + 1) & 0x00ffffff))
                    : (nima->SP + operand),
                    nima->ACC);
        break;
    }
    case 0xfc: {
        nima->ACC = nima_getmem(nima,
                                (operand & 0x00800000)
                                ? (nima->FP - ((~operand + 1) & 0x00ffffff))
                                : (nima->FP + operand));
        break;
    }
    case 0xfd: {
        nima_setmem(nima,
                    (operand & 0x00800000)
                    ? (nima->FP - ((~operand + 1) & 0x00ffffff))
                    : (nima->FP + operand),
                    nima->ACC);
        break;
    }
    default: {
        printf("Error: Unknown instruction: ");
        printf("%08x: %02x %07x\n", nima->IAR - 1, opcode, operand);
        return 0;
    }
    }
    return 1;
}


void nima_run(struct nima_state *nima)
{
    while (nima_step(nima));
}



struct nima_state NIMA;

int main(int argc, char *argv[])
{
    FILE *f;
    
    if (argc >= 2) {
        nima_setup(&NIMA);
        f = fopen(argv[1], "rb");
        nima_load(&NIMA, f);
        fclose(f);
        nima_run(&NIMA);
    } else {
        printf("Oof! No input file!\n");
    }
    return 0;
}

