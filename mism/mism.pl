#!/usr/bin/perl

########################################################################
# MISM - A quick-and-dirty assembler for the MINT VM
#
# Usage:  mism.pl [input file] [output file]
####


use strict;
use warnings;

my $LABEL_PATTERN = '\w[\w\d_-]+';
my $NUMBER_PATTERN = '-?\d+|-?0x[\dabcdefABCDEF]+';



sub open_text_file
{
    my $path = shift;
    open (my $file, '<:encoding(UTF-8)', $path)
        or die "Could not open file '$path' $!";
    return $file;
}


sub put_byte
{
    my $compilation = shift;
    my $org = shift;
    my $byte = shift;

    $compilation->{'instructions'}->[$org] = $byte & 0xff;
}

sub put_instruction
{
    my $compilation = shift;
    my $org = shift;
    my $instr = shift;

    $compilation->{'instructions'}->[$org] = $instr & 0xff;
    $compilation->{'instructions'}->[$org + 1] = ($instr >> 8) & 0xff;
    $compilation->{'instructions'}->[$org + 2] = ($instr >> 16) & 0xff;
    $compilation->{'instructions'}->[$org + 3] = ($instr >> 24) & 0xff;
}


sub signfparam
{
    my $param = shift;
    if ($param < 0) {
        return (($param) & (0x007fffff)) | 0x00800000;
    } else {
        return $param & 0x00ffffff;
    }
}


sub assemble_instruction
{
    my $compilation = shift;
    my $org = shift;
    my $opcode = shift;
    my $value = shift;

    if ($opcode eq ".LIT") {
        put_instruction($compilation, $org, 0xffffffff & $value);
    } elsif ($opcode eq "LDC") {
        put_instruction($compilation, $org, 0x0fffffff & $value);
    } elsif ($opcode eq "LDV") {
        put_instruction($compilation, $org, 0x10000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "STV") {
        put_instruction($compilation, $org, 0x20000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "ADD") {
        put_instruction($compilation, $org, 0x30000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "AND") {
        put_instruction($compilation, $org, 0x40000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "OR") {
        put_instruction($compilation, $org, 0x50000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "XOR") {
        put_instruction($compilation, $org, 0x60000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "EQL") {
        put_instruction($compilation, $org, 0x70000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "JMP") {
        put_instruction($compilation, $org, 0x80000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "JMN") {
        put_instruction($compilation, $org, 0x90000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "LDIV") {
        put_instruction($compilation, $org, 0xa0000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "STIV") {
        put_instruction($compilation, $org, 0xb0000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "CALL") {
        put_instruction($compilation, $org, 0xc0000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "ADC") {
        put_instruction($compilation, $org, 0xd0000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "STV") {
        put_instruction($compilation, $org, 0xe0000000 | (0x0fffffff & $value));
    } elsif ($opcode eq "HALT") {
        put_instruction($compilation, $org, 0xf0000000);
    } elsif ($opcode eq "NOT") {
        put_instruction($compilation, $org, 0xf1000000 | (0x00ffffff & $value));
    } elsif ($opcode eq "RAR") {
        put_instruction($compilation, $org, 0xf2000000 | (0x00ffffff & $value));
    } elsif ($opcode eq "RET") {
        put_instruction($compilation, $org, 0xf3000000);
    } elsif ($opcode eq "LDRA") {
        put_instruction($compilation, $org, 0xf4000000);
    } elsif ($opcode eq "STRA") {
        put_instruction($compilation, $org, 0xf5000000);
    } elsif ($opcode eq "LDSP") {
        put_instruction($compilation, $org, 0xf6000000);
    } elsif ($opcode eq "STSP") {
        put_instruction($compilation, $org, 0xf7000000);
    } elsif ($opcode eq "LDFP") {
        put_instruction($compilation, $org, 0xf8000000);
    } elsif ($opcode eq "STFP") {
        put_instruction($compilation, $org, 0xf9000000);
    } elsif ($opcode eq "LDRS") {
        put_instruction($compilation, $org, 0xfa000000 | signfparam($value));
    } elsif ($opcode eq "STRS") {
        put_instruction($compilation, $org, 0xfb000000 | signfparam($value));
    } elsif ($opcode eq "LDRF") {
        put_instruction($compilation, $org, 0xfc000000 | signfparam($value));
    } elsif ($opcode eq "STRF") {
        put_instruction($compilation, $org, 0xfd000000 | signfparam($value));
    } else {
        print "Instruction not found: $opcode!\n";
        die;
    }
}


sub parse_number
{
    my $arg = shift;
    if ($arg =~ /^-?0x([\dabcdefABCDEF]+)$/) {
        $arg = hex($arg);
    }
    return $arg;
}


sub assemble_prepare_value
{
    my $compilation = shift;
    my $pattern = shift;
    my $callback = shift;

    if ($pattern =~ /^${NUMBER_PATTERN}$/) {
        $callback->(parse_number($pattern));
    } elsif ($pattern =~ /^(${LABEL_PATTERN})$/) {
        my $address = $compilation->{'labels'}->{$pattern};
        if (defined ($address)) {
            $callback->($address);
        } else {
            unless (defined($compilation->{'label_waiters'}->{$pattern})) {
                $compilation->{'label_waiters'}->{$pattern} = [$callback];
            } else {
                push @{$compilation->{'label_waiters'}->{$pattern}}, $callback;
            }
        }
    } else {
        print "Error! Can not parse value: $pattern\n";
        die;
    }
}


sub assemble_line
{
    my $compilation = shift;
    my $instruction = shift;
    
    if ($instruction =~ /^([^;]*)\s*;(.*)$/) {
        $instruction = $1;
    }
    $instruction =~ s/^\s+|\s+$//g;

    if ($instruction =~ /^\s*$/) {
    } elsif ($instruction =~ /^\s*.include\s+(\S+)\s*$/) {
        include_file($compilation, $1);
    } elsif ($instruction =~ /^\s*(${LABEL_PATTERN}):\s*(.+)?$/) {
        my $label = $1;
        my $rest = $2;
        $compilation->{'labels'}->{$label} = $compilation->{'org'};
        if (defined($compilation->{'label_waiters'}->{$label})) {
            my $org = $compilation->{'org'};
            foreach my $callback (@{$compilation->{'label_waiters'}->{$label}}) {
                $callback->($org);
            }
        }
        delete $compilation->{'label_waiters'}->{$label};
        assemble_line($compilation, defined($rest)?$rest:'');  # Assemble rest of line
    } elsif ($instruction =~ /^\s*\.reg\s+(IAR|ACC|RA|SP|FP|IO)\s+(.+)\s*$/) {
        my $reg = $1;
        assemble_prepare_value($compilation, $2, sub {
            my $value = shift;
            $compilation->{'registers'}->{$reg} = $value;
                               });
    } elsif ($instruction =~ /^\s*\.org\s+(${NUMBER_PATTERN})\s*$/) {
        $compilation->{'org'} = parse_number($1);
    } elsif ($instruction =~ /^\s*\.str\s+"(.*)"\s*$/) {
        foreach my $char (split //, $1) {
            $compilation->{'instructions'}->[$compilation->{'org'}++] = ord($char);
        }
    } elsif ($instruction =~ /^\s*\.file\s+"(.*)"\s*$/) {
        my $fh = open_text_file($1);
        while (my $line = <$fh>) {
            foreach my $char (split //, $line) {
                $compilation->{'instructions'}->[$compilation->{'org'}++] = ord($char);
            }
        }
        close $fh;
    } elsif ($instruction =~ /^\s*\.byte\s+(${NUMBER_PATTERN})\s*$/) {
        my $pattern = parse_number($1);
        my $org = $compilation->{'org'}++;
        if ($pattern < 0) {
            $compilation->{'instructions'}->[$org] = ((-$pattern) & 0x7f) | 0x80;
        } else {
            $compilation->{'instructions'}->[$org] = $pattern & 0xff;
        }
    } elsif ($instruction =~ /^\s*(\w+)\s*$/) {
        assemble_instruction($compilation, $compilation->{'org'}, $1, 0);
        $compilation->{'org'} += 4;
    } elsif ($instruction =~ /^\s*([\w.]+)\s+(.+)\s*$/) {
        my $org = $compilation->{'org'};
        $compilation->{'org'} += 4;
        my $cmd = uc $1;
        assemble_prepare_value($compilation, $2, sub {
            my $value = shift;
            assemble_instruction($compilation, $org, $cmd, $value);
                       });
    } else {
        print "Oof, syntax error:\n$instruction\n";
        die;
    }
}


sub include_file
{
    my $compilation = shift;
    my $filename = shift;

    my $file = open_text_file($filename);

    while (my $instruction = <$file>) {
        chomp($instruction);
        assemble_line($compilation, $instruction);
    }

    close($file);
}


sub write_word
{
    my $out = shift;
    my $word = shift;
    print $out chr(($word) & 0xff);
    print $out chr(($word >> 8) & 0xff);
    print $out chr(($word >> 16) & 0xff);
    print $out chr(($word >> 24) & 0xff);
}


sub write_output
{
    my $compilation = shift;
    my $out = shift;

    write_word($out, $compilation->{'registers'}->{'IO'});
    write_word($out, $compilation->{'registers'}->{'IAR'});
    write_word($out, $compilation->{'registers'}->{'ACC'});
    write_word($out, $compilation->{'registers'}->{'RA'});
    write_word($out, $compilation->{'registers'}->{'SP'});
    write_word($out, $compilation->{'registers'}->{'FP'});

    foreach my $word (@{$compilation->{'instructions'}}) {
        if (defined($word)) {
            print $out chr($word);
        } else {
            print $out chr(0);
        }
    }
}


sub start_assembling
{
    my $input = shift;
    my $output = shift;

    my %compilation = (
        org => 0,
        instructions => [],
        labels => {},
        label_waiters => {},
        registers => {
            IAR => 0,
            ACC => 0,
            RA => 0,
            SP => 0,
            FP => 0,
            IO => 0xffffffff
        }
        );

    include_file(\%compilation, $input);

    foreach my $label (keys %{$compilation{'label_waiters'}}) {
        die "Undefined label: $label\n";
    }

    open(my $out, '>:raw', $output) or die "Could not open output file!";
    write_output(\%compilation, $out);
    close($out);
}


start_assembling($ARGV[0], $ARGV[1]);
