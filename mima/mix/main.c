#include <stdio.h>
#include <stdint.h>
#include <string.h>


#define TRUNC20(X) ((X) & (0x0fffff))
#define TRUNC24(X) ((X) & (0xffffff))

typedef uint32_t mima_word_t;



int load_word(FILE *file, mima_word_t *target)
{
    unsigned short i;
    unsigned char ch;
    mima_word_t word;

    word = 0;
    for (i = 0; i < 3; i++) {
        if (!fread(&ch, 1, 1, file)) return 0;
        word <<= 8;
        word |= ch;
    }
    
    *target = word;
    return 1;
}


void save_word(FILE *file, mima_word_t word)
{
    unsigned char ch;
    ch = (word >> 16);  fwrite(&ch, 1, 1, file);
    ch = (word >>  8);  fwrite(&ch, 1, 1, file);
    ch = (word);        fwrite(&ch, 1, 1, file);
}



struct mima_state
{
    mima_word_t IAR;
    mima_word_t ACC;
    mima_word_t RA;
    mima_word_t SP;
    mima_word_t FP;
    mima_word_t MEMORY[0x1000000];

    mima_word_t io_register;
};


void mima_setup(struct mima_state *mima)
{
    memset(&(mima->MEMORY), 0, sizeof(mima->MEMORY));
    mima->io_register = ~0;  // Can not be reached
}

mima_word_t mima_getmem(struct mima_state *mima, mima_word_t addr)
{
    if (TRUNC24(addr) == mima->io_register) {
        return TRUNC24(getchar());
    } else {
        return TRUNC24(mima->MEMORY[TRUNC24(addr)]);
    }
}

void mima_setmem(struct mima_state *mima, mima_word_t addr, mima_word_t value)
{
    if (TRUNC24(addr) == mima->io_register) {
        putchar(TRUNC24(value));
        fflush(stdout);
    } else {
        mima->MEMORY[TRUNC24(addr)] &= ~0xffffff;
        mima->MEMORY[TRUNC24(addr)] |= TRUNC24(value);
    }
}


void mima_save(struct mima_state *state, FILE *file)
{
    unsigned int i;
    
    save_word(file, state->IAR);
    save_word(file, state->ACC);
    save_word(file, state->RA);
    save_word(file, state->SP);
    save_word(file, state->FP);

    for (i = 0; i < 0x1000000; i++) {
        save_word(file, state->MEMORY[i]);
    }
}


int mima_load(struct mima_state *state, FILE *file)
{
    unsigned int i;
    
    if (!(load_word(file, &state->IAR)
          && load_word(file, &state->ACC)
          && load_word(file, &state->RA)
          && load_word(file, &state->SP)
          && load_word(file, &state->FP))) {
        printf("Error: Invalid file format!\n");
        return 0;
    }

    for (i = 0; i < 0x1000000; i++) {
        if (!load_word(file, &state->MEMORY[i]))
            break;
    }

    return 1;
}


int mima_step(struct mima_state *mima)
{
    unsigned char opcode;
    mima_word_t instruction;
    mima_word_t operand;

    instruction = mima_getmem(mima, mima->IAR++);
    
    if ((instruction & 0xf00000) == 0xf00000) {
        opcode = 0xf0 | ((instruction & 0x0f0000) >> 16);
        operand = (instruction & 0x00ffff);
    } else {
        opcode = ((instruction & 0xf00000) >> 20);
        operand = (instruction & 0x0fffff);
    }

    /*printf("%06x: %02x %05x  /  %06x\n",
      mima->IAR - 1, opcode, operand, mima->ACC);*/

    switch (opcode) {
    case 0x00: mima->ACC = operand; break;
    case 0x01: mima->ACC = mima_getmem(mima, operand); break;
    case 0x02: mima_setmem(mima, operand, mima->ACC); break;
    case 0x03: mima->ACC = TRUNC24(mima->ACC + mima_getmem(mima, operand)); break;
    case 0x04: mima->ACC = TRUNC24(mima->ACC & mima_getmem(mima, operand)); break;
    case 0x05: mima->ACC = TRUNC24(mima->ACC | mima_getmem(mima, operand)); break;
    case 0x06: mima->ACC = TRUNC24(mima->ACC ^ mima_getmem(mima, operand)); break;
    case 0x07: {
        mima->ACC = TRUNC24((mima->ACC == mima_getmem(mima, operand)) ? -1 : 0);
        break;
    }
    case 0x08: mima->IAR = operand; break;
    case 0x09: if (mima->ACC & 0x80000) mima->IAR = operand; break;
    case 0x0a: mima->ACC = mima_getmem(mima, mima_getmem(mima, operand)); break;
    case 0x0b: mima_setmem(mima, mima_getmem(mima, operand), mima->ACC); break;
    case 0x0c: mima->RA = mima->IAR; mima->IAR = operand; break;
    case 0x0d: {
        if (operand & 0x80000) {
            mima->ACC = TRUNC24(mima->ACC - (TRUNC20(~operand + 1)));
        } else {
            mima->ACC = TRUNC24(mima->ACC + operand);
        }
        break;
    }
    case 0xf0: mima->IAR--; return 0;
    case 0xf1: mima->ACC = TRUNC24(~(mima->ACC)); break;
        // TODO: Don't shift, rotate!
    case 0xf2: mima->ACC = TRUNC24(mima->ACC >> 1); break;
    case 0xf3: mima->IAR = mima->RA; break;
    case 0xf4: mima->ACC = mima->RA; break;
    case 0xf5: mima->RA = mima->ACC; break;
    case 0xf6: mima->ACC = mima->SP; break;
    case 0xf7: mima->SP = mima->ACC; break;
    case 0xf8: mima->ACC = mima->FP; break;
    case 0xf9: mima->FP = mima->ACC; break;
    case 0xfa: {
        mima->ACC = mima_getmem(mima,
                                (operand & 0x8000)
                                ? (mima->SP - (~operand + 1))
                                : (mima->SP + operand));
        break;
    }
    case 0xfb: {
        mima_setmem(mima,
                    (operand & 0x8000)
                    ? (mima->SP - (~operand + 1))
                    : (mima->SP + operand),
                    mima->ACC);
        break;
    }
    case 0xfc: {
        mima->ACC = mima_getmem(mima,
                                (operand & 0x8000)
                                ? (mima->FP - (~operand + 1))
                                : (mima->FP + operand));
        break;
    }
    case 0xfd: {
        mima_setmem(mima,
                    (operand & 0x8000)
                    ? (mima->FP - (~operand + 1))
                    : (mima->FP + operand),
                    mima->ACC);
        break;
    }
    default: {
        printf("Error: Unknown instruction: ");
        printf("%06x: %02x %05x\n", mima->IAR - 1, opcode, operand);
        return 0;
    }
    }
    return 1;
}


void mima_run(struct mima_state *mima)
{
    while (mima_step(mima));
}



struct mima_state MIMA;

int main(int argc, char *argv[])
{
    FILE *f;
    
    if (argc >= 2) {
        mima_setup(&MIMA);
        if (argc >= 3 && !strcmp(argv[2], "--io")) {
            MIMA.io_register = 0xffffff;
        }
        f = fopen(argv[1], "rb");
        mima_load(&MIMA, f);
        fclose(f);
        mima_run(&MIMA);
    } else {
        printf("Oof! No input file!\n");
    }
    return 0;
}

